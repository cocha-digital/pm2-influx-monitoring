require('dotenv').config();
const Influx = require('influx');

const url = `https:\/\/${process.env.DB_USER}:${process.env.DB_PASS}@${process.env.DB_HOST}/${process.env.DB_NAME}`;

const influxModel = new Influx.InfluxDB(url);
    influxModel.schema = [
      {
        measurement: 'pm2-prod-node',
        fields: {
          NAME:Influx.FieldType.STRING,
          CPU:Influx.FieldType.FLOAT,
          MEM:Influx.FieldType.FLOAT,
          PROCESS_ID: Influx.FieldType.INTEGER
        },
        tags: [
	        'process',
          'host'
        ]
      }
    ];

  module.exports = influxModel;
